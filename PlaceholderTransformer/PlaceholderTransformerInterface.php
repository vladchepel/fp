<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

interface PlaceholderTransformerInterface
{
    public function transform(mixed $value): mixed;
}