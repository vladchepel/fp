<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

final readonly class IntegerTransformer implements PlaceholderTransformerInterface
{
    public function transform(mixed $value): int
    {
        return (int) $value;
    }
}