<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

use InvalidArgumentException;

final readonly class GenericTransformer implements PlaceholderTransformerInterface
{
    private const array ALLOWED_TYPES = [
        'string',
        'integer',
        'array',
        'double',
        'boolean',
        'NULL',
    ];

    public function transform(mixed $value): mixed
    {
        $type = gettype($value);

        if (!in_array($type, self::ALLOWED_TYPES)) {
            throw new InvalidArgumentException('Invalid type provided');
        }

        return match (gettype($value)) {
            'integer', 'boolean' => (int) $value,
            'double' => (float) $value,
            'string' => "'$value'",
            'array' => sprintf('(%s)', implode(', ', $value)),
            'NULL' => 'NULL',
            default => $value,
        };
    }
}