<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

final readonly class IdentifierTransformer implements PlaceholderTransformerInterface
{
    public function transform(mixed $value): string
    {
        if (is_array($value)) {
            return implode(', ', array_map(static fn($value) => "`$value`", $value));
        }

        return "`$value`";
    }
}