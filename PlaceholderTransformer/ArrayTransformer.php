<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

use InvalidArgumentException;

final readonly class ArrayTransformer implements PlaceholderTransformerInterface
{
    public function __construct(
        private GenericTransformer $genericTransformer,
    ) {
    }

    public function transform(mixed $value): string
    {
        if (!is_array($value)) {
            throw new InvalidArgumentException('Type "array" is expected');
        }

        if (array_is_list($value)) {
            return implode(', ', $value);
        }

        return implode(
            ', ',
            array_map(
                fn($key, $value) => "`$key` = {$this->genericTransformer->transform($value)}",
                array_keys($value),
                $value,
            )
        );
    }
}