<?php

declare(strict_types=1);

namespace FpDbTest\PlaceholderTransformer;

final readonly class DecimalTransformer implements PlaceholderTransformerInterface
{
    public function transform(mixed $value): float
    {
        return (float) $value;
    }
}