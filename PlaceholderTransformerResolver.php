<?php

declare(strict_types=1);

namespace FpDbTest;

use FpDbTest\PlaceholderTransformer\PlaceholderTransformerInterface;
use InvalidArgumentException;

final readonly class PlaceholderTransformerResolver
{
    public function __construct(
        private array $transformers,
    ) {
    }

    public function resolve(string $placeholder): PlaceholderTransformerInterface
    {
        return $this->transformers[$placeholder]
            ?? throw new InvalidArgumentException("Unknown placeholder '$placeholder'");
    }
}