<?php

use FpDbTest\Database;
use FpDbTest\DatabaseTest;
use FpDbTest\PlaceholderTransformer\ArrayTransformer;
use FpDbTest\PlaceholderTransformer\DecimalTransformer;
use FpDbTest\PlaceholderTransformer\GenericTransformer;
use FpDbTest\PlaceholderTransformer\IdentifierTransformer;
use FpDbTest\PlaceholderTransformer\IntegerTransformer;
use FpDbTest\PlaceholderTransformerResolver;

spl_autoload_register(function ($class) {
    $a = array_slice(explode('\\', $class), 1);
    if (!$a) {
        throw new Exception();
    }
    $filename = implode('/', [__DIR__, ...$a]) . '.php';
    require_once $filename;
});

$mysqli = @new mysqli('127.0.0.1', 'root', 'password', 'database', 3306);
if ($mysqli->connect_errno) {
    throw new Exception($mysqli->connect_error);
}

$genericTransformer = new GenericTransformer();
$placeholderTransformerResolver = new PlaceholderTransformerResolver(
    [
        '?d' => new IntegerTransformer(),
        '?f' => new DecimalTransformer(),
        '?a' => new ArrayTransformer($genericTransformer),
        '?#' => new IdentifierTransformer(),
        '?' => $genericTransformer,
    ],
);
$db = new Database($mysqli, $placeholderTransformerResolver);
$test = new DatabaseTest($db);
$test->testBuildQuery();

exit('OK');
