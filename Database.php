<?php

declare(strict_types=1);

namespace FpDbTest;

use InvalidArgumentException;
use mysqli;

final readonly class Database implements DatabaseInterface
{
    public function __construct(
        private mysqli $mysqli,
        private PlaceholderTransformerResolver $placeholderTransformerResolver,
    ) {
    }

    public function buildQuery(string $query, array $args = []): string
    {
        $regexp = '/\?[dfa#]?/';

        if (preg_match_all($regexp, $query, $placeholders) !== 0 && empty($args)) {
            throw new InvalidArgumentException('Placeholder found in query but no arguments provided');
        }

        $placeholders = $placeholders[0];

        if (count($placeholders) !== count($args)) {
            throw new InvalidArgumentException('Number of placeholders does not match the number of arguments');
        }

        $matchCount = 0;

        $query = preg_replace_callback(
            pattern: $regexp,
            callback: function ($match) use (&$args) {
                $placeholder = $match[0];
                $arg = array_shift($args);

                if ($arg === $this->skip()) {
                    return $arg;
                }

                return $this->placeholderTransformerResolver
                    ->resolve($placeholder)
                    ->transform($arg);
            },
            subject: $query,
            count: $matchCount,
        );

        return $this->handleBlock($query);
    }

    public function skip(): string
    {
        return '_SKIP';
    }

    private function handleBlock(string $query): string
    {
        $query = preg_replace_callback(
            pattern: '/{(.*?)}/',
            callback: function ($block) use ($query) {
                if ($this->shouldSkipBlock($block[1])) {
                    return '';
                }

                return $block[1];
            },
            subject: $query
        );

        return $query;
    }

    public function shouldSkipBlock(string $block): bool
    {
        return str_contains($block, $this->skip());
    }
}
